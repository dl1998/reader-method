package com.example

import org.jenkinsci.plugins.workflow.cps.CpsScript
import static groovy.json.JsonOutput.*

/**
 * A class that handle references within dictionaries ({@code Map}) and read content from shared library resources.
 * Support YAML and JSON formats, check file extension to recognize how to proceed further.
 *
 * @see Map
 * @since 1.0
 * @author Dmytro Leshchenko
 */
class ReferenceMap {
    private String[] files
    private Map content
    private CpsScript context

    /**
     * Public constructor for {@code ReferenceMap}, take {@code WorkflowScript} from pipeline and list of files to read.
     * {@code WorkflowScript} can be passed as {@code this}.
     *
     * @see WorkflowScript
     * @param context the {@code WorkflowScript} context class instance from the pipeline
     * @param files list of files, these files will be parsed
     * @since 1.0
     * @author Dmytro Leshchenko
     */
    ReferenceMap(CpsScript context, String... files) {
        this.context = context
        this.files = files
    }

    /**
     * Method that shall be called after class instance creation. This method parse all files that were passed to the
     * class constructor and merge their content into one dictionary ({@code Map}).
     *
     * @see Map
     * @see WorkflowScript#readYaml
     * @see WorkflowScript#readJSON
     * @see WorkflowScript#libraryResource
     * @throws Exception If {@code this.files} contains at least one file that has unsupported extension (supported
     *                   extensions: .yaml and .json)
     * @since 1.0
     * @author Dmytro Leshchenko
     */
    void parseFiles() throws Exception {
        ArrayList content = []
        for (String file in this.files) {
            def temporaryData = this.context.libraryResource file
            if (file.endsWith('.yaml')) {
                content.add(this.context.readYaml(text: temporaryData))
            } else if (file.endsWith('.json')) {
                content.add(this.context.readJSON(text: temporaryData, returnPojo: true))
            } else {
                throw new Exception("Unsupported file format to read, file: ${file}.")
            }
        }
        this.mergeMaps(content)
    }

    /**
     * Take {@code List} of {@code Map} objects and merge them all into one {@code Map} object.
     *
     * @param maps {@code List} of {@code Map} object
     * @see List
     * @see Map
     * @since 1.0
     * @author Dmytro Leshchenko
     */
    void mergeMaps(List<Map> maps) {
        if (maps.size() == 1) {
            this.content = maps[0]
        } else {
            for (int index = 1; index < maps.size(); index++) {
                if (this.content == null) {
                    this.content = mergeTwoMap(maps[index - 1].clone(), maps[index].clone())
                } else {
                    this.content = mergeTwoMap(this.content, maps[index].clone())
                }
            }
        }
    }

    /**
     * Merge two {@code Map} objects into one {@code Map} object. If both dictionaries contains the same fields, then
     * fields from {@code sourceMap} will override appropriate fields from {@code destinationMap}.
     *
     * @param destinationMap a map into which fields will be merged (from: {@code sourceMap})
     * @param sourceMap a map from which fields will be taken to merge them into {@code destinationMap}
     * @return final map with all fields, after overriding fields with the same name
     * @see Map
     * @since 1.0
     * @author Dmytro Leshchenko
     */
    Map mergeTwoMap(Map destinationMap, Map sourceMap) {
        if (sourceMap instanceof Map) {
            for (element in sourceMap) {
                if (element.value instanceof Map) {
                    if (destinationMap[element.key] != null) {
                        mergeTwoMap(destinationMap[element.key], element.value)
                    } else {
                        destinationMap.put(element.key, element.value)
                    }
                } else {
                    destinationMap.remove(element.key)
                    destinationMap.put(element.key, element.value)
                }
            }
        }
        return destinationMap
    }

    /**
     * A method walk through all elements of the {@code Map} object and replace references with real values.
     *
     * @since 1.0
     * @author Dmytro Leshchenko
     */
    void parseContent() {
        this.parseContent(this.content)
    }

    /**
     * A method walk through all elements of the {@code Map} object and replace references with real values.
     *
     * @param content reference to the
     *                {@code Map}|{@code List}|{@code String}|{@code Double}|{@code Integer}|{@code Boolean} object
     * @since 1.0
     * @author Dmytro Leshchenko
     */
    void parseContent(def content) {
        if (content instanceof Map) {
            for (def element in content) {
                if (element.value instanceof Map || element.value instanceof List) {
                    parseContent(element.value)
                } else {
                    def value = element.value
                    def newValue = null
                    while(value != newValue) {
                        value = content[element.key]
                        content[element.key] = extractValue(content[element.key])
                        newValue = content[element.key]
                    }
                }
            }
        } else if (content instanceof List) {
            def index = 0
            for (def element in content) {
                def value = element
                def newValue = null
                while(value != newValue) {
                    value = content[index]
                    content[index] = extractValue(content[index])
                    newValue = content[index]
                }
                index += 1
            }
        }
    }

    /**
     * Extract value from the {@code String}|{@code Integer}|{@code Boolean} object if it contains reference. Support
     * references to another key in the {@code Map} object or to environment variables.
     *
     * @param data {@code String}|{@code Double}|{@code Integer}|{@code Boolean} object that potentially contains
     *             reference
     * @return {@code String}|{@code Double}|{@code Integer}|{@code Boolean} object, where references replaced with real
     *         value
     * @see #getEnvironmentVariable
     * @since 1.0
     * @author Dmytro Leshchenko
     */
    def extractValue(def data) {
        def match = data =~ /\{(\S*?)\}/
        if (match.size() > 0) {
            for (found in match) {
                String variable = found[1]
                def value
                if (variable.startsWith('env.')) {
                    String envVariable = variable.replace('env.', '')
                    value = this.context.getEnvironmentVariable(envVariable)
                } else {
                    value = this.get(variable)
                }
                if (value != null) {
                    data = data.replace("{${variable}}", value)
                }
            }
        }
        return data
    }

    /**
     * A method that return element from the dictionary ({@code Map}) object.
     *
     * @param path is a path to the element that we want to receive from the {@code Map} object, where every level is
     *        separated by '.'. For example: if we want to receive element from a second level, then we shall use the
     *        following syntax {@code this.get("first-level.second-level")}, where "first-level" and "second-level" are
     *        keys from the dictionary
     * @param defaultValue if element was not found, then {@code defaultValue} will be returned, by default
     *        {@code defaultValue} is null
     * @return element from the final (merged) dictionary or default value
     * @since 1.0
     * @author Dmytro Leshchenko
     */
    def get(def path, def defaultValue = null) {
        String[] splitPath = path.split('\\.')
        def result = this.content
        for (def part in splitPath) {
            result = result[part]
            if (result == null) {
                break
            }
        }
        if (result == null) {
            result = defaultValue
        }
        return result
    }

    /**
     * A method that set a new value in the dictionary ({@code Map}) object.
     *
     * @param path a path to the element that we want to update in the {@code Map} object, where every level is
     *        separated by '.'. For example: if we want to update element from a second level, then we shall use the
     *        following syntax {@code this.set("first-level.second-level", "new value")}, where "first-level" and
     *        "second-level" are keys from the dictionary and second parameter is a new {@code value} that will be
     *        assigned
     * @param value a new value that will be assigned to the {@code path}, can be one of the following types:
     *        {@code String}|{@code Double}|{@code Integer}|{@code Boolean}|{@code Map}|{@code List}
     * @since 1.0
     * @author Dmytro Leshchenko
     */
    void set(String path, def value) {
        String[] splitPath = path.split('\\.')
        def result = this.content
        if (splitPath.size() > 1) {
            for (int index = 0; index < splitPath.size() - 1; index++) {
                if (result[splitPath[index]] == null) {
                    result[splitPath[index]] = [:]
                }
                result = result[splitPath[index]]
            }
        }
        String lastPart = splitPath[splitPath.size() - 1]
        result[lastPart] = value
    }

    /**
     * Print all key-values available within final {@code Map} object.
     *
     * @since 1.0
     * @author Dmytro Leshchenko
     */
    void print() {
        this.context.println(prettyPrint(toJson(this.content)))
        //this.context.println(this.content)
    }

    /**
     * Dump data from final {@code Map} object into specified file.
     *
     * @param path a path to the file where data suppose to be dumped.
     * @since 1.0
     * @author Dmytro Leshchenko
     */
    void dump(String path) {
        if (path.endsWith(".yaml")) {
            this.context.writeYaml(data: this.content, file: path, overwrite: true)
        } else if (path.endsWith(".json")) {
            this.context.writeJSON(json: this.content, file: path, pretty: true)
        } else {
            throw new Exception("Unsupported file format to write, file: ${file}.")
        }
    }
}
