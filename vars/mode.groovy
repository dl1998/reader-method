def call(Map parameters) {
    Map<String, Closure> modesClosure = parameters.modesClosure ?: [:]
    def reference = parameters.reference
    String workflow = parameters.workflow
    String mode = reference.get("workflow.${workflow}.mode", 'method')

    String workflowDescription = workflow.replaceAll('\\.', ' ').replaceAll('-', ' ').toLowerCase()

    List<String> defaultDescriptionLines = [
            "[WARNING] Command for ${workflowDescription} was not found!",
            "[HINT] Define 'workflow.${workflow}.command' in your yaml/json file."
            ]

    String defaultDescription = defaultDescriptionLines.join('\r\n')

    println("[INFO] Selected mode: ${mode}")
    switch (mode) {
        case "batch":
            String defaultCommand = "echo ${defaultDescription} & exit /b 1"
            bat(script: reference.get("workflow.${workflow}.command", defaultCommand),
                    label: reference.get("workflow.${workflow}.label"))
            break
        case "shell":
            String defaultCommand = "echo \"${defaultDescription}\" ; exit 1"
            sh(script: reference.get("workflow.${workflow}.command", defaultCommand),
                    label: reference.get("workflow.${workflow}.label"))
            break
        default:
            if (modesClosure.containsKey(mode)) {
                modesClosure[mode]()
            } else {
                throw new Exception('Unsupported mode was selected!')
            }
            break
    }
}