def call(String name) {
    def environment = env[name]
    if (environment != null) {
        return environment
    } else {
        return System.getenv(name)
    }
}